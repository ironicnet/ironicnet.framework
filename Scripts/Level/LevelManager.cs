﻿using System.Linq;
using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;

namespace Ironicnet.Framework.Level
{
    public class LevelManager : MonoBehaviour
    {
        public ResourceFile Resource;
        public string ResourceFile;
        public Dictionary<string, GameObject> Prefabs = new Dictionary<string, GameObject>();

        public Dictionary<string, Dictionary<string, Sprite>> Sprites = new Dictionary<string, Dictionary<string, Sprite>>();

        public event System.Action<ResourceFile> ResourceFileLoaded;
        public string LevelName = null;
        public Level Level;
        public Transform LevelContainer;

        public LevelConfig DefaultLevelConfig ;

        protected string ResourcePath;

        public static LevelManager Instance
        {
            get;
            protected set;
        }

        public LevelManager()
        {
            Instance = this;
        }
        // Use this for initialization
        protected virtual void Start()
        {
            LoadResourceFile();
        }

        // Update is called once per frame
        protected virtual void Update()
        {
        }
        [ContextMenu("LoadResourceFile")]
        protected virtual void LoadResourceFile()
        {
            LoadResourceFile(GetResourceFile());
        }
        
        protected virtual string GetResourcePath()
        {
            /*
            #if UNITY_EDITOR
                        resourcesPath = "Assets/Resources/";
            #elif UNITY_STANDALONE
                             // You cannot add a subfolder, at least it does not work for me
                        resourcesPath = "MyGame_Data/Resources/";
            #endif
             * */
            return Path.Combine(Application.dataPath, "Resources\\");
        }
        [ContextMenu("UpdateResourcePath")]
        protected virtual string GetResourceFile()
        {
            ResourcePath = Path.Combine(GetResourcePath(), ResourceFile);
            /*
            #if UNITY_EDITOR
                        resourcesPath = "Assets/Resources/";
            #elif UNITY_STANDALONE
                             // You cannot add a subfolder, at least it does not work for me
                        resourcesPath = "MyGame_Data/Resources/";
            #endif
             * */
            return ResourcePath;
        }

        protected virtual void LoadResourceFile(string path, bool loadLevel=true)
        {
            Logging.Logger.GlobalInstance.LogFormat("Loading resource file: {0}. Load Level: {1}", path, loadLevel);
            ReadResourceFile(path);

            LoadPrefabsInstances();
            LoadSprites();
            if (loadLevel)
            {
                if (string.IsNullOrEmpty(LevelName))
                {
                    NewLevel();
                }
                else
                {
                    LoadLevel();
                }
            }

            if (ResourceFileLoaded != null)
            {
                ResourceFileLoaded.Invoke(Resource);
            }
            Loaded = true;
        }

        public void NewLevel()
        {
            DefaultLevelConfig.Layers = Resource.Layers;
            Level = new Level()
            {
                Config = DefaultLevelConfig,
                ResourceFile = ResourceFile,
            };
            Level.LoadConfig();
        }
        public void LoadLevel()
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(LevelSerialized));
            string resourcesPath = GetResourcePath();
            LevelSerialized serialized = null;
            Logging.Logger.GlobalInstance.LogFormat("Reading level from: {0}", System.IO.Path.Combine(resourcesPath, "Level.xml"));
            using (System.IO.StreamReader file = new System.IO.StreamReader(System.IO.Path.Combine(resourcesPath, "Level.xml")))
            {
                serialized = serializer.Deserialize(file) as LevelSerialized;
                file.Close();
            }
            ResourcePath = Path.Combine(resourcesPath, serialized.ResourceFile);
            Level = new Level()
            {
                Config = serialized.Config,
                ResourceFile = serialized.ResourceFile
            };
            LoadResourceFile(ResourcePath, false);
            Level.LoadConfig();

            for (int i = LevelContainer.childCount - 1; i >= 0; i--)
            {
                GameObject.Destroy(LevelContainer.GetChild(i).gameObject);
            }
            for (int i = 0; i < serialized.TileObjects.Count; i++)
            {
                TileInstance instance = serialized.TileObjects[i];
                ResourceLayer layer = GetLayer(instance.Tile);
                PlaceTileOnPosition(instance.Tile, instance.TileX, instance.TileY, layer);
            }
        }

        public ResourceLayer GetLayer(ResourceTile tile)
        {
            for (int i = 0; i < Resource.Layers.Count; i++)
            {
                if (Resource.Layers[i].Name == tile.LayerName)
                {
                    return Resource.Layers[i];
                }
            }
            return Resource.Layers[0];
        }


        public void SaveLevel()
        {
            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(LevelSerialized));
            string resourcesPath = GetResourcePath();
            Logging.Logger.GlobalInstance.LogFormat("Saving level to: {0}", System.IO.Path.Combine(resourcesPath, "Level.xml"));
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(System.IO.Path.Combine(resourcesPath, "Level.xml")))
            {
                writer.Serialize(file, Level.GetSerialized());
                file.Close();
            }
        }

        protected virtual void ReadResourceFile(string path)
        {
            var serializer = new XmlSerializer(typeof(ResourceFile));
            using (var stream = new System.IO.FileStream(path, FileMode.Open))
            {
                Resource = serializer.Deserialize(stream) as ResourceFile;
                stream.Close();
            }
        }


        protected virtual void LoadPrefabsInstances()
        {
            Prefabs.Clear();
            foreach (var prefab in Resource.Prefabs)
            {
                var prefabInstance = Resources.Load(prefab.ResourcePath);
                if (prefabInstance != null)
                {
                    Prefabs.Add(prefab.Name, prefabInstance as GameObject);
                }
            }
        }

        protected virtual void LoadSprites()
        {
            Sprites.Clear();
            foreach (var tile in Resource.Tiles)
            {
                LoadSpritesFromTile(tile);
            }
        }

        protected virtual void LoadSpritesFromTile(ResourceTile tile)
        {
            if (tile != null)
            {
                if (!string.IsNullOrEmpty(tile.ResourcePath) && !Sprites.ContainsKey(tile.ResourcePath))
                {
                    var spriteDict = new Dictionary<string, Sprite>();
                    var spritesInResource = Resources.LoadAll<Sprite>(tile.ResourcePath);

                    for (int i = 0; i < spritesInResource.Length; i++)
                    {
                        string spriteName = GetSpriteName(tile, spritesInResource[i].name);
                        //Debug.Log(string.Format("Style:{0} i:{1}: Name:{2}. Exists: {3}", style.Name, i, spriteName, spriteDict.ContainsKey(spriteName)));
                        spriteDict.Add(spriteName, spritesInResource[i]);
                    }
                    Sprites.Add(tile.ResourcePath, spriteDict);
                }
            }
        }

        public virtual Sprite GetSprite(ResourceTile tile)
        {
            LoadSpritesFromTile(tile);

            string spriteName = GetSpriteName(tile);
            if (!string.IsNullOrEmpty(spriteName) && Sprites.ContainsKey(tile.ResourcePath) && Sprites[tile.ResourcePath].ContainsKey(spriteName))
            {
                return Sprites[tile.ResourcePath][spriteName];
            }
            else
            {
                return null;
            }

        }

        private static string GetSpriteName(ResourceTile tile)
        {
            string spriteName = null;
            if (tile != null)
            {
                spriteName = tile.SpriteName;
            }
            return spriteName;
        }
        private static string GetSpriteName(ResourceTile tile, string name)
        {
            string spriteName = null;
            if (tile != null)
            {
                spriteName = name;
            }
            return spriteName;
        }

        private GameObject InstancePrefab(string prefabName)
        {
            if (Prefabs.ContainsKey(prefabName))
            {
                return GameObject.Instantiate(Prefabs[prefabName]);
            }
            else
            {
                return null;
            }
        }

        public bool Loaded { get; protected set; }


        public virtual TileInstance PlaceTileOnPosition(ResourceTile tile, int tileX, int tileY, ResourceLayer layer)
        {
            //Debug.Log(string.Format("PlaceTileOnPosition: TileX: {0}. TileY: {1}.", tileX, tileY));
            for (int y = 0; y < tile.Rowspan; y++)
            {
                for (int x = 0; x < tile.Colspan; x++)
                {
                    DeleteTileOnPosition(tileX + x, tileY + y, layer.Order);
                }
            }
            //Debug.Log(string.Format("PlaceTileOnPosition: Instancing Prefab: {0} ({1} / {2})", tile.PrefabName, tile.Name, style!=null ?  style.Name : "NO STYLE"));
            TileInstance instance = new TileInstance();
            instance.Tile = tile;
            instance.TileX = tileX;
            instance.TileY = tileY;
            instance.GameObject = GetPrefab(tile, tileX, tileY, layer.Order);
            if (instance.GameObject == null)
            {
                Debug.Log(string.Format("PlaceTileOnPosition: Prefab not instantiated"));
                return null;
            }
            else
            {
                instance.NotifyApplyTile();
            }


            //Debug.Log(string.Format("PlaceTileOnPosition: Filling Prefab: {0} ({1} / {2})", tile.PrefabName, tile.Name, style != null ? style.Name : "NO STYLE"));
            for (int y = 0; y < tile.Rowspan; y++)
            {
                for (int x = 0; x < tile.Colspan; x++)
                {
                    SetTileInstance(instance, tileX + x, tileY + y, layer.Order);
                }
            }
            return instance;
        }
        public virtual void SetTileInstance(TileInstance instance, int targetX, int targetY, int targetZ)
        {
            if (targetX < Level.TileObjects.GetUpperBound(0) && targetY < Level.TileObjects.GetUpperBound(1))
            {
                Debug.Log(string.Format("Setting instance: {0},{1},{2}", targetX, targetY, targetZ), this);
               Level.TileObjects[targetX, targetY, targetZ-1] = instance;
            }
        }
        public virtual TileInstance GetTileInstance(int targetX, int targetY, int targetZ)
        {
            if (!(targetX <= Level.TileObjects.GetUpperBound(0) && targetY <= Level.TileObjects.GetUpperBound(1) && targetZ <= Level.TileObjects.GetUpperBound(2)))
            {
                Debug.Log(string.Format("Overflow: {0},{1},{2}. Max: {3},{4},{5}", targetX, targetY, targetZ, Level.TileObjects.GetUpperBound(0), Level.TileObjects.GetUpperBound(1), Level.TileObjects.GetUpperBound(2)));
            }
            return Level.TileObjects[targetX, targetY, targetZ-1];
        }

        public virtual void DeleteTileOnPosition(int tileX, int tileY, int tileZ)
        {
            //Debug.Log(string.Format("DeleteTileOnPosition: TileX: {0} ({2}). TileY: {1} ({3}).", tileX, tileY, TileObjects.GetUpperBound(0), TileObjects.GetUpperBound(1)));
            var currentObject = GetTileInstance(tileX, tileY, tileZ);
            if (currentObject != null)
            {
                //Debug.Log(string.Format("DeleteTileOnPosition: Obj: {0}.", currentObject.Tile.Name), currentObject.GameObject);
                for (int y = 0; y < currentObject.Tile.Rowspan; y++)
                {
                    for (int x = 0; x < currentObject.Tile.Colspan; x++)
                    {
                        GameObject.Destroy(currentObject.GameObject);
                        SetTileInstance(null, currentObject.TileX + x, currentObject.TileY + y, tileZ);
                    }
                }
            }
            else
            {
                //Debug.Log("DeleteTileOnPosition: Empty.");
            }
        }
        protected virtual GameObject GetPrefab(ResourceTile tile, int tileX, int tileY, int tileZ)
        {
            if (tile.PrefabName != null)
            {
                GameObject obj = InstancePrefab(tile.PrefabName);
                if (obj == null) return null;
                obj.transform.parent = LevelContainer;
                obj.name = tile.Name + "(" + tileX.ToString() + ", " + tileY.ToString() + ", " + tileZ.ToString() + ")";
                obj.transform.position = new Vector3(tileX * Level.Config.TileWidth, tileY * Level.Config.TileHeight, tileZ * Level.Config.TileWidth);
                return obj;
            }
            return null;
        }

    }
}
