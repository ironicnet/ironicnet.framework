﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using UnityEngine;

namespace Ironicnet.Framework.Level
{
    [Serializable]
    public class TileInstance
    {
        public ResourceTile Tile;
        [NonSerialized]
        [XmlIgnore]
        public GameObject GameObject;
        public int TileX = -1;
        public int TileY = -1;

        internal void NotifyApplyTile()
        {
            GameObject.SendMessage("ApplyTile", this, SendMessageOptions.DontRequireReceiver);
        }
    }
}
