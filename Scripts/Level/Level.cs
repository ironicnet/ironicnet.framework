﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Ironicnet.Framework.Level
{
    [Serializable]
    public class Level
    {
        public LevelConfig Config = new LevelConfig();
        public TileInstance[, ,] TileObjects;
        public string ResourceFile;

        public Level()
        {

        }

        public void LoadSerialized(LevelSerialized serialized)
        {
            Config = serialized.Config;
            ResourceFile = serialized.ResourceFile;
            LoadConfig();
        }
        public void LoadConfig()
        {
            TileObjects = new TileInstance[Config.MapWidth, Config.MapHeight, Config.Layers.Count + 1];
        }

        public LevelSerialized GetSerialized()
        {
            LevelSerialized level = new LevelSerialized()
            {
                Config = Config,
                ResourceFile = ResourceFile
            };

            for (int x = 0; x < TileObjects.GetUpperBound(0); x++)
            {
                for (int y = 0; y < TileObjects.GetUpperBound(1); y++)
                {
                    for (int z = 0; z < TileObjects.GetUpperBound(2); z++)
                    {
                        level.TileObjects.Add(TileObjects[x, y, z]);
                    }
                }
            }
            return level;
        }
    }

    [Serializable]
    public class LevelSerialized
    {
        public LevelConfig Config = new LevelConfig();
        [XmlArray("Tiles")]
        [XmlArrayItem("Tile")]
        public List<TileInstance> TileObjects = new List<TileInstance>();
        public string ResourceFile;
    }
}
