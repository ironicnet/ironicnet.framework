﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ironicnet.Framework.Level
{
    [Serializable]
    public class LevelConfig
    {
        public float TileWidth = 0.16f;
        public float TileHeight = 0.16f;
        public int MapWidth = 500;
        public int MapHeight = 500;

        public List<ResourceLayer> Layers = new List<ResourceLayer>();
    }
}
