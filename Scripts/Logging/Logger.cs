﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Ironicnet.Framework.Logging
{
    public class Logger
    {
        public enum LogLevel
        {
            DEBUG = 0,
            INFO = 1,
            WARN = 2,
            ERROR = 3,
            EXCEPTION = 4
        }
        protected static Logger _globalInstance;

        public static Logger GlobalInstance
        {
            get
            {
                if (_globalInstance == null)
                {
                    _globalInstance = new Logger("Global");
                }
                return _globalInstance;
            }
        }
        public void Log(object message)
        {
            if (Level > LogLevel.DEBUG) return;
            Debug.Log(message);
        }
        public void Log(object message, UnityEngine.Object context)
        {
            if (Level > LogLevel.DEBUG) return;
            Debug.Log(message, context);
        }
        public void LogFormat(string format, params object[] args)
        {
            if (Level > LogLevel.DEBUG) return;
            Debug.LogFormat(format, args);
        }
        public void LogFormat(UnityEngine.Object context, string format, params object[] args)
        {
            if (Level > LogLevel.DEBUG) return;
            Debug.LogFormat(context, format, args);
        }

        public void LogInfo(object message)
        {
            if (Level > LogLevel.INFO) return;
            Debug.Log(message);
        }
        public void LogInfo(object message, UnityEngine.Object context)
        {
            if (Level > LogLevel.INFO) return;
            Debug.Log(message, context);
        }
        public void LogInfoFormat(string format, params object[] args)
        {
            if (Level > LogLevel.INFO) return;
            Debug.LogFormat(format, args);
        }
        public void LogInfoFormat(UnityEngine.Object context, string format, params object[] args)
        {
            if (Level > LogLevel.INFO) return;
            Debug.LogFormat(context, format, args);
        }

        public void LogWarning(object message)
        {
            if (Level > LogLevel.WARN) return;
            Debug.LogWarning(message);
        }
        public void LogWarning(object message, UnityEngine.Object context)
        {
            if (Level > LogLevel.WARN) return;
            Debug.LogWarning(message, context);
        }
        public void LogWarningFormat(string format, params object[] args)
        {
            if (Level > LogLevel.WARN) return;
            Debug.LogWarningFormat(format, args);
        }
        public void LogWarningFormat(UnityEngine.Object context, string format, params object[] args)
        {
            if (Level > LogLevel.WARN) return;
            Debug.LogWarningFormat(context, format, args);
        }
        public void LogError(object message)
        {
            if (Level > LogLevel.ERROR) return;
            Debug.LogError(message);
        }
        public void LogError(object message, UnityEngine.Object context)
        {
            if (Level > LogLevel.ERROR) return;
            Debug.LogError(message, context);
        }
        public void LogErrorFormat(string format, params object[] args)
        {
            if (Level > LogLevel.ERROR) return;
            Debug.LogErrorFormat(format, args);
        }
        public void LogErrorFormat(UnityEngine.Object context, string format, params object[] args)
        {
            if (Level > LogLevel.ERROR) return;
            Debug.LogErrorFormat(context, format, args);
        }
        public void LogException(Exception exception)
        {
            if (Level > LogLevel.EXCEPTION) return;
            Debug.LogException(exception);
        }
        public void LogException(Exception exception, UnityEngine.Object context)
        {
            if (Level > LogLevel.EXCEPTION) return;
            Debug.LogException(exception, context);
        }

        private string _name;

        public string Name
        {
            get { return _name; }
        }

        public LogLevel Level
        {
            get;
            protected set;
        }

        public Logger(string name)
        {
            this._name = name;
#if UNITY_EDITOR
            Level = Logger.LogLevel.DEBUG;
#else
            Level = Logger.LogLevel.INFO;
#endif
        }
    }
}
