﻿using UnityEngine;
using System.Collections;

namespace Ironicnet.Framework.CameraControl
{
    public class CameraFollow2D : MonoBehaviour
    {

        public Transform target;
        public enum Axis
        {
            XOnly,
            YOnly,
            XAndY
        }

        public float XSpeed = 1;
        public float YSpeed = 2;
        /*
         * Buggy!
        public bool UseSnapping = false;
        public float SnapX = 1f;
        public float SnapY = 1f;
        public float SnapZ = 1f;
        */
        [Range(0,1)]
        public float TopY = 0.95f;
        [Range(0, 1)]
        public float BottomY = 0.05f;
        [Range(0, 1)]
        public float LeftX = 0.05f;
        [Range(0, 1)]
        public float RightX = 0.95f;

        public Axis FollowAxis = Axis.XAndY;
        // Use this for initialization

        void Start()
        {
            Camera = UnityEngine.Camera.main;
        }
        Vector3 targetScreenPosition;
        Vector3 movement = new Vector3();
        protected Camera Camera;

        // Update is called once per frame
        void Update()
        {
            movement = new Vector3();
            if (target != null)
            {
                targetScreenPosition = Camera.WorldToViewportPoint(target.position);
                if (FollowAxis == Axis.XAndY || FollowAxis == Axis.XOnly)
                {
                    if (targetScreenPosition.x < LeftX)
                    {
                        movement.x -= Time.deltaTime * XSpeed;
                    }
                    if (targetScreenPosition.x > RightX)
                    {
                        movement.x += Time.deltaTime * XSpeed;
                    }
                }

                if (FollowAxis == Axis.XAndY || FollowAxis == Axis.YOnly)
                {
                    if (targetScreenPosition.y > TopY)
                    {
                        movement.y += Time.deltaTime * YSpeed;
                    }
                    if (targetScreenPosition.y < BottomY)
                    {
                        movement.y -= Time.deltaTime * YSpeed;
                    }
                }
                /*
                if (UseSnapping)
                {
                    movement = new Vector3(
                    Mathf.Round(movement.x / SnapX) * SnapX,
                    Mathf.Round(movement.y / SnapY) * SnapY,
                    Mathf.Round(movement.z / SnapZ) * SnapZ);
                }
                 * */
                transform.Translate(movement);
            }
        }

        void OnDrawGizmosSelected()
        {
            if (target != null)
            {
                Gizmos.DrawSphere(target.position, 0.01f);
            }
            var topLeft = Camera.main.ViewportToWorldPoint(new Vector3(LeftX, TopY, 0));
            var topRight = Camera.main.ViewportToWorldPoint(new Vector3(RightX, TopY, 0));
            var bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(LeftX, BottomY, 0));
            var bottomRight = Camera.main.ViewportToWorldPoint(new Vector3(RightX, BottomY, 0));
            Gizmos.DrawSphere(topLeft, 0.05f);
            Gizmos.DrawSphere(topRight, 0.05f);
            Gizmos.DrawSphere(bottomLeft, 0.05f);
            Gizmos.DrawSphere(bottomRight, 0.05f);
            if (FollowAxis == Axis.XAndY || FollowAxis == Axis.YOnly)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(topLeft, topRight);
                Gizmos.DrawLine(bottomLeft, bottomRight);
            }
            if (FollowAxis == Axis.XAndY || FollowAxis == Axis.XOnly)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(topRight, bottomRight);
                Gizmos.DrawLine(bottomLeft, topLeft);
            }
        }
    }

}