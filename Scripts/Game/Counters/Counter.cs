﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ironicnet.Framework.Game.Counters
{
    [Serializable]
    public class Counter
    {
        public string Name;
        public float CurrentAmount =0;

        public Counter(string counterName)
        {
            this.Name = counterName;
        }
    }
}
