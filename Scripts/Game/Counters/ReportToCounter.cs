﻿using UnityEngine;
using System.Collections;

namespace Ironicnet.Framework.Game.Counters
{
    public class ReportToCounter : MonoBehaviour
    {
        public string CounterName;
        public float Amount;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Report()
        {
            Ironicnet.Framework.Game.GameManager.Instance.AddToCounter(CounterName, Amount);
        }
    }
}
