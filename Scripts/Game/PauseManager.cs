﻿using UnityEngine;
using System.Collections;

namespace Ironicnet.Framework.Game
{
    public class PauseManager : MonoBehaviour
    {
        public UnityEngine.Events.UnityEvent Pausing;
        public UnityEngine.Events.UnityEvent Resuming;
        // Use this for initialization
        void Start()
        {

        }
        void OnEnable()
        {
            Pause();
        }
        void OnDisable()
        {
            Time.timeScale = 1;
        }
        // Update is called once per frame
        void Update()
        {

        }

        public void Pause()
        {
            Time.timeScale = 0;
            if (Pausing != null)
                Pausing.Invoke();
        }
        public void Resume()
        {
            if (Resuming != null)
                Resuming.Invoke();
        }
    }
}