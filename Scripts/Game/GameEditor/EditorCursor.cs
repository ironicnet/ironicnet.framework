﻿using UnityEngine;
using System.Collections;
using Vectrosity;
using UnityEngine.UI;
using Ironicnet.Framework.Level;

namespace Ironicnet.Framework.Game.GameEditor
{
    public class EditorCursor : MonoBehaviour
    {
        public Vector2 TilePosition;
        public ResourceTile SelectedTile;


        public Rect GridRect;
        public Material lineMaterial;
        public float lineWidth = 1;
        public LineType lineType = new LineType();
        protected VectorLine line;


        public bool Visible = true;

        public SpriteRenderer Renderer;

        void Awake()
        {
            GridRect = new Rect(0, 0, 0.16f, 0.16f);
            line = new VectorLine(gameObject.name, new Vector3[5], lineMaterial, lineWidth, lineType);

            if (Renderer == null)
            {
                Renderer = transform.GetComponentInChildren<SpriteRenderer>();
            }
        }
        // Use this for initialization
        void Start()
        {
            if (Renderer == null)
            {
                Renderer = transform.GetComponentInChildren<SpriteRenderer>();
            }
        }

        // Update is called once per frame
        void Update()
        {

            Render();
        }
        void Render()
        {
            UpdateVisibility();
            Draw();
        }

        public void UpdateVisibility()
        {
            line.active = Visible;
        }
        public Vector3[] points;
        public CursorPCController Controller;
        [ContextMenu("Draw")]
        void Draw()
        {
            line.MakeRect(
                            new Vector3(transform.position.x + GridRect.x, transform.position.y + GridRect.y),
                            new Vector3(transform.position.x + GridRect.x + GridRect.width, transform.position.y + GridRect.y + GridRect.height)
                        );
            line.Draw();
            points = line.points3.ToArray();
        }


        public void SetPosition(Vector2 position)
        {
            if (TilePosition != position)
            {
                TilePosition = position;
                transform.position = new Vector3(TilePosition.x * LevelManager.Instance.Level.Config.TileWidth, TilePosition.y * LevelManager.Instance.Level.Config.TileHeight, 0);
            }
        }

        internal void SetTile(ResourceTile tile)
        {

            SelectedTile = tile;
            GridRect.width = tile.Colspan * LevelManager.Instance.Level.Config.TileWidth;
            GridRect.height = tile.Rowspan * LevelManager.Instance.Level.Config.TileHeight;
            Renderer.transform.localPosition = new Vector3(GridRect.width / 2, GridRect.height / 2, 0);
            if (!string.IsNullOrEmpty(tile.SpriteName))
            {
                Renderer.sprite = LevelManager.Instance.GetSprite(tile);
                Renderer.enabled = true;
            }
            else
            {
                Renderer.sprite = null;
                Renderer.enabled = false;
            }
        }
    }

}