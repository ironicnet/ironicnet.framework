﻿using UnityEngine;
using System.Collections;
using Ironicnet.Framework.Level;


namespace Ironicnet.Framework.Game.GameEditor
{
    [RequireComponent(typeof(EditorCursor))]
    public class CursorPCController : MonoBehaviour
    {

        EditorCursor Cursor;
        EditorManager.EditorInput input;

        TileInstance SelectedInstance;
        // Use this for initialization
        void Start()
        {
            Cursor = this.GetComponent<EditorCursor>();
            Cursor.Controller = this;
        }

        Vector3 worldPosition;
        // Update is called once per frame
        void Update()
        {
            if (EditorManager.Instance.Enabled)
            {
                worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                var tiledPosition = new Vector3(Mathf.FloorToInt(worldPosition.x / LevelManager.Instance.Level.Config.TileWidth),
                                                                    Mathf.FloorToInt(worldPosition.y / LevelManager.Instance.Level.Config.TileHeight));

                if (tiledPosition.x >= 0 && tiledPosition.y >= 0 && tiledPosition.x < LevelManager.Instance.Level.Config.MapWidth && tiledPosition.y < LevelManager.Instance.Level.Config.MapHeight)
                {
                    Cursor.TilePosition = new Vector2(tiledPosition.x, tiledPosition.y);
                    Cursor.transform.position = new Vector3(Cursor.TilePosition.x * LevelManager.Instance.Level.Config.TileWidth,
                                                    Cursor.TilePosition.y * LevelManager.Instance.Level.Config.TileHeight);
                }
                EditorManager.Instance.UpdateCursor();
            }
        }

        public void OnDrawGizmos()
        {
            Gizmos.DrawSphere(worldPosition, 0.5f);
        }

        public void OnGUI()
        {
            /*float top = 50f;
            GUI.Label(new Rect(top, 50f, 100f, 50f), worldPosition.ToString());
            top += 100f;
            GUI.Label(new Rect(top, 50f, 100f, 50f), new Vector3(Mathf.FloorToInt(worldPosition.x / LevelManager.Instance.Level.Config.TileWidth),
                                                                Mathf.FloorToInt(worldPosition.y / LevelManager.Instance.Level.Config.TileHeight)).ToString());
            top += 100f;
            GUI.Label(new Rect(top, 50f, 100f, 50f), new Vector3(worldPosition.x / LevelManager.Instance.Level.Config.TileWidth,
                                                                worldPosition.y / LevelManager.Instance.Level.Config.TileHeight).ToString());
             * */
        }


        internal EditorManager.EditorInput GetInput()
        {
            input = new EditorManager.EditorInput()
            {
                ButtonPlacePressed = Input.GetMouseButton(0),
                ButtonPlaceDown = Input.GetMouseButtonDown(0),
                ButtonDeletePressed = Input.GetMouseButton(1),
                ButtonDeleteDown = Input.GetMouseButtonDown(1),
                ButtonSelectDown = Input.GetMouseButtonDown(2),
                ButtonNextTileDown = Input.GetAxis("Mouse ScrollWheel")>0,
                ButtonPreviousTileDown = Input.GetAxis("Mouse ScrollWheel") < 0,
            };
            return input;
        }
    }
}