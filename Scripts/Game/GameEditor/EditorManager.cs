﻿using UnityEngine;
using System.Collections;
using System;
using Vectrosity;
using Ironicnet.Framework.Level;

namespace Ironicnet.Framework.Game.GameEditor
{
    public class EditorManager : MonoBehaviour
    {

        public static EditorManager Instance
        {
            get;
            protected set;
        }
        public EditorManager()
        {
            Debug.Log("Setting EditorManager Instance", this);
            Instance = this;
        }
        private float inputTimer = 0;
        protected VectorLine LevelOutline;
        protected Rect LevelRect;

        protected int CurrentTileIndex;
        protected int CurrentStyleIndex;
        public ResourceTile CurrentSelectedTile = null;
        protected ResourceTile LastSelectedTile;
        public ResourceLayer CurrentLayer;
        protected ResourceLayer LastLayer;
        protected EditorInput input;

        public EditorCursor Cursor;
        public bool Enabled = false;

        public float inputDelay = 1f;

        public Material lineMaterial;
        public float lineWidth = 1;
        public LineType lineType = new LineType();
        public UnityEngine.UI.Text InfoText;

        public event Action SelectedLayerChanged;
        public event Action SelectedTileChanged;
        public event Action TilePlaced;
        public event Action TileDeleted;
        public event Action<TileInstance> TileSelected;
        // Use this for initialization
        protected virtual void Start()
        {
            LevelManager.Instance.ResourceFileLoaded += Instance_ResourceFileLoaded;
            if (LevelManager.Instance.Loaded)
            {
                Init();
            }
        }

        void Instance_ResourceFileLoaded(ResourceFile obj)
        {
            Init();
        }

        void Init()
        {
            LevelOutline = new VectorLine(this.name, new Vector3[8], lineMaterial, lineWidth, lineType);
            Cursor.SetPosition(new Vector2(Mathf.CeilToInt(LevelManager.Instance.Level.Config.MapWidth / 2), 10));
            LevelRect = new Rect(0, 0, LevelManager.Instance.Level.Config.MapWidth * LevelManager.Instance.Level.Config.TileWidth, LevelManager.Instance.Level.Config.MapHeight * LevelManager.Instance.Level.Config.TileHeight);
            Enabled = true;
            SetCursorCurrentTile();
        }
        [Serializable]
        public class EditorInput
        {
            //public float HorizontalAxis;
            //public float VerticalAxis;
            public bool ButtonNextTileDown;
            public bool ButtonPreviousTileDown;
            public bool ButtonSelectDown;
            public bool ButtonCancelDown;
            public bool ButtonPlaceDown;
            public bool ButtonDeleteDown;
            public bool ButtonPlacePressed;
            public bool ButtonDeletePressed;
        }
        // Update is called once per frame
        protected virtual void Update()
        {
            if (Cursor.Visible != Enabled)
            {
                Cursor.Visible = Enabled;
                Cursor.UpdateVisibility();
            }
            Cursor.gameObject.SetActive(Enabled);

            if (Enabled)
            {
                if (Cursor.Controller == null)
                {
                    UpdateCursor();
                }
                DrawLevelOutline();
            }
        }

        internal void UpdateCursor()
        {
            if (Enabled)
            {
                input = GetInput();

                if (input.ButtonNextTileDown || input.ButtonPreviousTileDown)
                {
                    CycleNextTile(input.ButtonNextTileDown);
                }
                if (Cursor.Controller == null)
                {
                    HandleCursorPosition();
                }

                if (input.ButtonDeletePressed)
                {
                    DeleteTileOnCursor();
                }
                else if (input.ButtonPlacePressed)
                {
                    var currentObject = LevelManager.Instance.GetTileInstance((int)Cursor.TilePosition.x, (int)Cursor.TilePosition.y, CurrentLayer.Order);
                    Debug.Log(string.Format("Current Object: {0}", (currentObject != null ? (currentObject.Tile != null ? currentObject.Tile.Name : "NO TILE") : "NONE")));
                    //If there is a different tile at that position, we delete it.
                    if (currentObject != null && currentObject.Tile != CurrentSelectedTile)
                    {
                        DeleteTileOnCursor();
                    }

                    //If there isn't any tile in that position, we place the tile there
                    if (currentObject == null)
                    {
                        currentObject = PlaceTileOnCursor(CurrentSelectedTile);
                    }
                    else
                    {
                        //if there is a tile alrady there, we just change the tile and style to match the selection
                        currentObject.Tile = CurrentSelectedTile;
                        currentObject.NotifyApplyTile();
                    }
                }
                else if (input.ButtonSelectDown)
                {
                    var currentObject = LevelManager.Instance.GetTileInstance((int)Cursor.TilePosition.x, (int)Cursor.TilePosition.y, CurrentLayer.Order);
                }
            }
        }

        private void HandleCursorPosition()
        {

            PositionCursor(Input.GetAxis("EditorHorizontal"), Input.GetAxis("EditorVertical"));
        }

        protected virtual void DrawLevelOutline()
        {


            LevelOutline.MakeRect(
                            new Vector3(transform.position.x + LevelRect.x, transform.position.y + LevelRect.y),
                            new Vector3(transform.position.x + LevelRect.x + LevelRect.width, transform.position.y + LevelRect.y + LevelRect.height)
                        );
            LevelOutline.Draw();
        }

        protected virtual TileInstance PlaceTileOnCursor(ResourceTile tile)
        {
            int x = (int)Cursor.TilePosition.x;
            int y = (int)Cursor.TilePosition.y;
            TileInstance prefab = LevelManager.Instance.PlaceTileOnPosition(tile, x, y, CurrentLayer);
            return prefab;
        }

        protected virtual void DeleteTileOnCursor()
        {
            int x = (int)Cursor.TilePosition.x;
            int y = (int)Cursor.TilePosition.y;
            LevelManager.Instance.DeleteTileOnPosition(x, y, CurrentLayer.Order);
        }

        protected virtual EditorInput GetInput()
        {
            if (Cursor.Controller != null)
            {
                return Cursor.Controller.GetInput();
            }
            else
            {
                EditorInput input = new EditorInput();
                //input.HorizontalAxis = Input.GetAxis("EditorHorizontal");
                //input.VerticalAxis = Input.GetAxis("EditorVertical");
                input.ButtonNextTileDown = Input.GetButtonDown("EditorNextTile");
                input.ButtonPreviousTileDown = Input.GetButtonDown("EditorPrevTile");
                input.ButtonSelectDown = Input.GetButtonDown("EditorSelect");
                input.ButtonCancelDown = Input.GetButtonDown("EditorCancel");
                input.ButtonPlaceDown = Input.GetButtonDown("EditorPlace");
                input.ButtonPlacePressed = Input.GetButton("EditorPlace");
                input.ButtonDeleteDown = Input.GetButtonDown("EditorDelete");
                input.ButtonDeletePressed = Input.GetButton("EditorDelete");
                return input;
            }
        }

        protected virtual void PositionCursor(float xAxis, float yAxis)
        {
            inputTimer -= Time.deltaTime;
            if (inputTimer <= 0)
            {
                Vector2 targetPosition = Cursor.TilePosition + new Vector2(xAxis == 0 ? 0 : (xAxis > 0 ? 1 : -1), yAxis == 0 ? 0 : (yAxis > 0 ? 1 : -1));
                if (targetPosition.x >= 0 && targetPosition.y >= 0 && targetPosition.x < LevelManager.Instance.Level.Config.MapWidth && targetPosition.y < LevelManager.Instance.Level.Config.MapHeight)
                {
                    inputTimer = inputDelay;
                    Cursor.SetPosition(targetPosition);

                }
            }
        }

        protected virtual void CycleNextTile(bool nextTile)
        {
            CurrentStyleIndex = 0;
            if (nextTile)
            {
                CurrentTileIndex++;
                if (CurrentTileIndex >= LevelManager.Instance.Resource.Tiles.Count)
                {
                    CurrentTileIndex = 0;
                }
            }
            else
            {
                CurrentTileIndex--;
                if (CurrentTileIndex < 0)
                {
                    CurrentTileIndex = LevelManager.Instance.Resource.Tiles.Count - 1;
                }
            }
            SetCursorCurrentTile();
        }
        protected virtual void SetCursorCurrentTile()
        {
            CurrentSelectedTile = LevelManager.Instance.Resource.Tiles[CurrentTileIndex];
            CurrentLayer = LevelManager.Instance.GetLayer(CurrentSelectedTile);
            Cursor.SetTile(CurrentSelectedTile);
            if (LastSelectedTile != CurrentSelectedTile)
            {
                if (SelectedTileChanged != null) SelectedTileChanged.Invoke();
            }
            else if (LastLayer != CurrentLayer)
            {
                if (SelectedLayerChanged != null) SelectedLayerChanged.Invoke();
            }

            LastSelectedTile = CurrentSelectedTile;
            LastLayer = CurrentLayer;
        }
    }

}