﻿using UnityEngine;
using System.Collections;
using System;


namespace Ironicnet.Framework.Game
{
    public class MusicPlayer : MonoBehaviour
    {

        public Ironicnet.Framework.Game.UI.MusicInfoDisplay MusicInfoDisplay;
        public AudioSource MusicSource;
        public MusicInfo[] Clips;
        public int CurrentTrackIndex = -1;
        public bool Repeat = false;
        public float DisplayTime = 5;
        public float FadeSpeed = 1;
        public float FadeStep = 0.1f;
        [Range(0, 1)]
        public float MusicVolume = 1;

        [Serializable]
        public class MusicInfo
        {
            public string TrackName;
            public string Artist;
            public AudioClip Clip;
        }
        // Use this for initialization
        void Start()
        {

            if (AutoPlay)
            {
                PlayCurrent();
            }
        }

        [ContextMenu("Play Next")]
        private void PlayNext()
        {
            CurrentTrackIndex++;
            PlayCurrent();

        }
        [ContextMenu("Play Current")]
        private void PlayCurrent()
        {
            if (CurrentTrackIndex == -1) CurrentTrackIndex = 0;
            if (CurrentTrackIndex >= Clips.Length) CurrentTrackIndex = 0;
            IsPlaying = MusicSource !=null;
            if (IsPlaying)
            {
                Play(Clips[CurrentTrackIndex]);

                MusicSource.Play();
            }
        }

        private void Play(MusicInfo musicInfo)
        {
            if (MusicSource != null)
            {
                MusicSource.Stop();
                MusicSource.clip = musicInfo.Clip;
            }
            //MusicSource.loop = Repeat;
            if (MusicInfoDisplay!=null) MusicInfoDisplay.Show(musicInfo, DisplayTime, FadeSpeed, FadeStep);

        }
        protected float MusicCurrentTime;
        protected float MusicTotalTime;
        // Update is called once per frame
        void Update()
        {
            if (MusicSource != null)
            {
                if (IsPlaying)
                {
                    MusicCurrentTime = MusicSource.time;
                    MusicTotalTime = MusicSource.clip.length;
                    if (MusicCurrentTime >= MusicTotalTime || !MusicSource.isPlaying)
                    {
                        if (Repeat)
                        {
                            PlayCurrent();
                        }
                        else
                        {
                            PlayNext();
                        }
                    }
                }
                MusicSource.volume = MusicVolume;
            }
        }

        public bool AutoPlay;
        private bool IsPlaying;

        public void SetVolumeFromSlider(UnityEngine.UI.Slider slider)
        {
            SetVolume(slider.value);

        }
        public void SetVolume(float value)
        {
            MusicVolume = value;
            if (MusicSource != null)
            {
                MusicSource.volume = MusicVolume;
            }
            PlayerPrefs.SetFloat("MusicVolume", value);
        }
    }

}