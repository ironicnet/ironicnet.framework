﻿using Ironicnet.Framework.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ironicnet.Framework.Game.Conditions
{
    [Serializable]
    public class CounterCondition : ConditionBase
    {
        public enum EvaluationType
        {
            Equal,
            Greater,
            GreaterOrEqual,
            Lower,
            LowerOrEqual,
            Distinct
        }
        public string CounterName;
        private float current;
        public float Amount;
        public EvaluationType Evaluator;

        public override ConditionType Type
        {
            get
            {
                return ConditionType.Counter;
            }
        }
        public override bool Check()
        {
            LastResult = Result;
            Counters.Counter counter = GameManager.Instance.GetCounter(string.IsNullOrEmpty(CounterName) ? Name : CounterName);
            current = 0;
            if (counter != null)
            {
                current = counter.CurrentAmount;
            }
            else
            {
                Logger.GlobalInstance.LogWarningFormat("Counter {0} / {1} not found", CounterName, Name);
            }
            switch (Evaluator)
            {
                case EvaluationType.Equal:
                    Result = current == Amount;
                    break;
                case EvaluationType.Greater:
                    Result = current > Amount;
                    break;
                case EvaluationType.GreaterOrEqual:
                    Result = current >= Amount;
                    break;
                case EvaluationType.Lower:
                    Result = current < Amount;
                    break;
                case EvaluationType.LowerOrEqual:
                    Result = current <= Amount;
                    break;
                case EvaluationType.Distinct:
                    Result = current != Amount;
                    break;
                default:
                    Logger.GlobalInstance.LogWarningFormat("Evaluator {0} not supported", Evaluator);
                    break;
            }
            return Result;
        }
    }
}
