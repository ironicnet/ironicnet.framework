﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ironicnet.Framework.Game.Conditions
{
    [Serializable]
    public class ConditionGroup : ConditionBase
    {
        public CounterCondition[] CounterConditions;
        public InteractableCondition[] InteractableConditions;
        public ManualCondition[] ManualConditions;
        private bool CountersOk;
        private bool InteractablesOk;
        private bool ManualsOk;
        public override ConditionType Type
        {
            get { return ConditionType.Multiple; }
        }

        public override bool Check()
        {
            LastResult = Result;
            CountersOk = CheckConditionsArray(CounterConditions);
            InteractablesOk = CheckConditionsArray(InteractableConditions);
            ManualsOk = CheckConditionsArray(ManualConditions);
            Logging.Logger.GlobalInstance.LogFormat("CountersOk: {0}. InteractablesOk: {1}. Manuals: {2}", CountersOk, InteractablesOk, ManualsOk);
            Result = CountersOk && InteractablesOk && ManualsOk;
            return Result;
        }

        private bool CheckConditionsArray(ConditionBase[] conditions)
        {
            bool allOk = true;
            if (conditions != null && conditions.Length > 0)
            {
                for (int i = 0; i < conditions.Length; i++)
                {
                    conditions[i].Check();
                    allOk = allOk && conditions[i].Result;
                }
            }
            return allOk;
        }
    }
}
