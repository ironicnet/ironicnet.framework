﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ironicnet.Framework.Game.Conditions
{
    public abstract class ConditionBase
    {
        public abstract ConditionType Type
        {
            get;
        }
        public string Name;
        public virtual bool Result { get; protected set; }
        public virtual bool LastResult { get; protected set; }
        public abstract bool Check();

        public UnityEngine.Events.UnityEvent ResultChangedToOk;
        public UnityEngine.Events.UnityEvent ResultChangedToFail;


        public virtual void RaiseEvents()
        {
            if (LastResult != Result)
            {
                if (Result)
                {
                    ResultChangedToOk.Invoke();
                }
                else
                {
                    ResultChangedToFail.Invoke();
                }
            }
        }
    }
}
