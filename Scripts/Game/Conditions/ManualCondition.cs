﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ironicnet.Framework.Game.Conditions
{
    [Serializable]
    public class ManualCondition : ConditionBase
    {
        public override ConditionType Type
        {
            get
            {
                return ConditionType.Manual;
            }
        }

        public override bool Check()
        {
            LastResult = Result;
            return Result;
        }
    }
}
