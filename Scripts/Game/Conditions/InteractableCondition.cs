﻿using Ironicnet.Framework.Game.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ironicnet.Framework.Game.Conditions
{
    [Serializable]
    public class InteractableCondition : ConditionBase
    {
        public Interactable Interactable;
        public bool MustBeActivated;

        public override ConditionType Type
        {
            get
            {
                return ConditionType.Interactable;
            }
        }
        public override bool Check()
        {
            LastResult = Result;
            Logging.Logger.GlobalInstance.LogFormat(Interactable, "Activated: {0}. Expected: {1}", Interactable.Activated, MustBeActivated);
            Result = Interactable.Activated == MustBeActivated;
            return Result;
        }
    }
}
