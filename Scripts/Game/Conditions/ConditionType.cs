﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ironicnet.Framework.Game.Conditions
{
    public enum ConditionType
    {
        Manual,
        Interactable,
        Counter,
        Multiple
    }
}
