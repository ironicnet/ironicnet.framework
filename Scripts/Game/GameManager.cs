﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using Ironicnet.Framework.Game.Conditions;
using System.Collections.Generic;
using Ironicnet.Framework.Game.Counters;

namespace Ironicnet.Framework.Game
{
    public class GameManager : MonoBehaviour
    {
        public float DefaultMusicVolume = 0.5f;

        public float CurrentTime;
        public PauseManager PauseManager;
        public MusicPlayer MusicManager;
        public UnityEngine.Events.UnityEvent WinEvent;
        public UnityEngine.Events.UnityEvent LooseEvent;
        public double ElapsedTime;

        public List<ConditionGroup> Conditions = new List<ConditionGroup>();
        public List<Counter> Counters = new List<Counter>();

        public bool GamePlaying = false;

        public enum Events
        {
            GameStart,
            GameStop,
            GamePause,
            GameResume
        }


        public static GameManager Instance
        {
            get;
            protected set;
        }
        public GameManager()
        {
            Instance = this;
        }
        // Use this for initialization
        void Start()
        {
            MusicManager.SetVolume(PlayerPrefs.GetFloat("MusicVolume", DefaultMusicVolume));

        }

        // Update is called once per frame
        void Update()
        {
            if (GamePlaying)
            {
                CurrentTime += Time.deltaTime;
                if (Input.GetButtonDown("Pause"))
                {
                    PauseGame();
                }
                for (int i = 0; i < Conditions.Count; i++)
                {
                    bool checkResult = Conditions[i].Check();
                    Logging.Logger.GlobalInstance.LogFormat("Checking condition: {0}. Result: {1}", Conditions[i].Name, checkResult);
                    
                    Conditions[i].RaiseEvents();
                }
            }
            if (Input.GetKeyDown(KeyCode.F5))
            {
                ToggleEditor();
            }
            if (Input.GetKeyDown(KeyCode.F9) && Ironicnet.Framework.Game.GameEditor.EditorManager.Instance.Enabled)
            {
                Ironicnet.Framework.Level.LevelManager.Instance.SaveLevel();
            }
            if (Input.GetKeyDown(KeyCode.F10))
            {
                Ironicnet.Framework.Level.LevelManager.Instance.LoadLevel();
            }
        }

        private void PauseGame()
        {
            RaiseEvent(Events.GamePause.ToString());

            if (PauseManager != null)
            {
                PauseManager.gameObject.SetActive(true);
            }
        }

        protected void ToggleEditor()
        {
            if (Ironicnet.Framework.Game.GameEditor.EditorManager.Instance != null)
            {
                Ironicnet.Framework.Game.GameEditor.EditorManager.Instance.Enabled = !Ironicnet.Framework.Game.GameEditor.EditorManager.Instance.Enabled;
                if (!GamePlaying)
                {
                    StartGame();
                }
                else
                {
                    EndGame();
                }
            }
        }


        public void SaveOptions()
        {
            PlayerPrefs.SetFloat("MusicVolume", MusicManager.MusicVolume);

            PlayerPrefs.Save();
        }
        public bool CanQuit
        {
            get
            {
                return !Application.isWebPlayer && !Application.isEditor;
            }
        }
        public void Quit()
        {
            EndGame();
            Application.Quit();
        }
        public void AddToCounter(string counterName, float amount)
        {
            Counter counter = GetCounter(counterName);
            if (counter == null)
            {
                counter = new Counter(counterName);
                Counters.Add(counter);
            }
            counter.CurrentAmount += amount;
        }

        public Counter GetCounter(string counterName)
        {
            return Counters.FirstOrDefault(c => c.Name.ToUpper() == counterName.ToUpper());
        }

        public void Success()
        {
            ElapsedTime = CurrentTime;
            WinEvent.Invoke();
        }
        public void Fail()
        {
            ElapsedTime = CurrentTime;
            StartCoroutine(ShowLooseScreen());
        }
        public float TimeBeforeDeathScreen = 2;
        public IEnumerator ShowLooseScreen()
        {
            yield return new WaitForSeconds(TimeBeforeDeathScreen);
            LooseEvent.Invoke();
        }

        [ContextMenu("Start Game")]
        public void StartGame()
        {
            GamePlaying = true;
            RaiseEvent(Events.GameStart.ToString());
        }
        [ContextMenu("End Game")]
        public void EndGame()
        {
            GamePlaying = false;
            RaiseEvent(Events.GameStop.ToString());
        }
        protected class EventListener
        {
            public Component Component { get; protected set; }
            public string MethodName { get; protected set; }
            public EventListener()
            {

            }
            public EventListener(Component component, string methodName)
            {
                Component = component;
                MethodName = methodName;
            }
        }
        protected Dictionary<string, List<EventListener>> _events = new Dictionary<string, List<EventListener>>();
        public void On(string eventName, Component component, string methodName)
        {
            if (!_events.ContainsKey(eventName.ToUpper()))
            {
                _events.Add(eventName.ToUpper(), new List<EventListener>());
            }
            Debug.Log(string.Format("Registering for Event {0}. Component: {1}. Method: {2}", eventName, component.name, methodName), this);
            _events[eventName.ToUpper()].Add(new EventListener(component, methodName));
        }

        private void RaiseEvent(string eventName)
        {
            if (_events.ContainsKey(eventName.ToUpper()))
            {
                var listeners = _events[eventName.ToUpper()];
                if (listeners.Count > 0)
                {
                    for (int i = 0; i < listeners.Count; i++)
                    {
                        Debug.Log(string.Format("Firing Event {0}. Component: {1}. Method: {2}", eventName, listeners[i].Component.name, listeners[i].MethodName), this);
                        listeners[i].Component.SendMessage(listeners[i].MethodName);
                    }
                }
                else
                {
                    Debug.Log(string.Format("Firing Event {0}. Zero Listeners.", eventName), this);
                }
            }
            else
            {
                Debug.Log(string.Format("Firing Event {0}. No Listeners.", eventName), this);
            }
        }

        public void RemoveOn(string eventName, Component component)
        {
            if (!_events.ContainsKey(eventName.ToUpper()))
            {
                _events[eventName.ToUpper()].RemoveAll(l => l.Component == component);
            }
        }
    }

}