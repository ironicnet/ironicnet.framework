﻿using UnityEngine;
using System.Collections;
using Ironicnet.Framework.Game.Entities;

namespace Ironicnet.Framework.Game.Entities
{
    public class BaseEntity : MonoBehaviour
    {
        public bool IsVisible = true;
        public Direction CurrentDirection = Direction.Right;
        public string CurrentDirectionText
        {
            get
            {
                return CurrentDirection.ToString();
            }
        }
        public bool IsDead;


        private bool _active = true;

        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {

        }

        public virtual System.Collections.Generic.IEnumerable<Vector3> GetViewSpots()
        {
            return new Vector3[1] { transform.position };
        }
        public void Hide()
        {
            IsVisible = false;
        }
        public void Show()
        {
            IsVisible = false;
        }

        public virtual void Kill()
        {
            Debug.Log("Killed", this);
            gameObject.SetActive(false);
        }
    }

}