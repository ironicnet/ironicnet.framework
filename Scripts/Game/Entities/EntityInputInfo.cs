﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ironicnet.Framework.Game.Entities
{
    [Serializable]
    public class EntityInputInfo
    {
        public float HorizontalAxis;
        public float VerticalAxis;
        public bool IsRunning;
        public bool IsPressingJump;
        public bool Log;
        public bool IsPressingActionDown;
        public bool IsPressingAction;
        public bool IsPressingAttack1Down;
        public bool FreezeInput;

        protected int currDirection;
        public bool IsPressingAttack2Down;
        public Direction HorizontalDirection
        {
            get
            {
                Direction direction = Direction.None;
                if (HorizontalAxis<0)
                {
                    
                    direction = Direction.Left;
                }
                else if (HorizontalAxis > 0)
                {
                    direction = Direction.Right;
                }
                currDirection = (int)direction;
                return direction;
            }
            set
            {
                throw new InvalidOperationException("Use horizontal axis");
            }
        }
    }
}
