﻿using Ironicnet.Framework.Game.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Ironicnet.Framework.Game.Entities
{
    public class Interactable : MonoBehaviour
    {
        private bool IsInside = false;
        public bool Activated = false;
        public bool ToggleActivation = false;
        public bool ActivateAfterDuration = false;

        public float ActionDuration;
        public void SetActionDuration(float duration)
        {
            ActionDuration = duration;
        }
        public bool CannotInteractAfterActivation = false;
        public bool CanBeInteracted = true;
        public ActionType ActionType = ActionType.Default;

        protected IEntityController interacted;

        public UnityEvent rejected;
        public UnityEvent activated;
        public UnityEvent deactivated;
        public UnityEvent playerEnterTrigger;
        public UnityEvent playerExitTrigger;
        public UnityEvent StartInteraction;
        public UnityEvent EndInteraction;

        public ConditionGroup Condition = new ConditionGroup();

        public bool CheckConditions(bool notify)
        {
            if (notify && (!Condition.Check()))
            {
                rejected.Invoke();
            }
            return Condition==null || Condition.Result;
        }
        public virtual void StartInteract(IEntityController caller)
        {
            interacted = caller;
            StartInteraction.Invoke();
        }
        public virtual void Interact(IEntityController caller)
        {
            interacted = caller;
            Activate();
            if (CannotInteractAfterActivation)
            {
                CanBeInteracted = false;
                this.GetComponent<Collider>().enabled = false;
            }
        }
        public virtual void EndInteract(IEntityController caller)
        {
            interacted = caller;
            EndInteraction.Invoke();
        }

        [ContextMenu("Activate")]
        public virtual void Activate()
        {
            if (!ToggleActivation || !Activated)
            {
                activated.Invoke();
            }
            else
            {
                deactivated.Invoke();
            }
            if (ToggleActivation)
                Activated = !Activated;
        }
        protected virtual void OnTriggerEnter(Collider other)
        {
            IsInside = true;

            playerEnterTrigger.Invoke();
        }
        protected virtual void OnTriggerExit(Collider other)
        {
            IsInside = false;

            playerExitTrigger.Invoke();
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color(1, 0.522f, 0);
            for (int i = 0; i < activated.GetPersistentEventCount(); i++)
            {
                var target = activated.GetPersistentTarget(i);
                if (target is Component)
                {
                    Gizmos.DrawLine(transform.position, (target as Component).transform.position);
                }
            }
        }

    }
}