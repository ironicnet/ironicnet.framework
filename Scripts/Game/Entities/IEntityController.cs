﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Ironicnet.Framework.Game.Entities
{
    public interface IEntityController
    {
         EntityInputInfo InputInfo { get; }

         UnityEngine.MonoBehaviour Behavior { get; }

         BaseEntity OwnerEntity { get; }
    }
}
