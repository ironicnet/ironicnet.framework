﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ironicnet.Framework.Game.Entities
{
    public enum Direction
    {
        None = 0,
        Right = 1,
        Left = -1
    }
}
