﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Ironicnet.Framework.Game.UI.Effects
{
    public class TypeWriterEffect : MonoBehaviour
    {
        [Multiline]
        public string TargetText;

        private string LeftText;

        public float TypingSpeed;

        public bool TypeOnStart = true;
        public AudioClip letterAudioClip;
        public AudioClip returnAudioClip;
        public AudioSource audioSource;

        private float typeInterval = 0;
        private int currentIndex;
        private UnityEngine.UI.Text textComponent;
        public bool ClearOnType = true;


        public void Start()
        {
            audioSource = this.GetComponent<AudioSource>();
            textComponent = this.GetComponent<UnityEngine.UI.Text>();
            if (TypeOnStart)
            {
                StartTyping(TargetText);
            }
        }

        public void Update()
        {
            if (LeftText != null && LeftText.Length > 0)
            {
                Type();
            }
        }
        public void StartTyping(string text)
        {
            TargetText = text;
            LeftText = text;
            if (ClearOnType && textComponent!=null)
            {
                textComponent.text = string.Empty;
            }
        }

        [ContextMenu("Start Typing")]
        public void StartTypingText()
        {
            LeftText = TargetText;
        }


        public void Type()
        {
            typeInterval += Time.deltaTime;
            if (typeInterval >= TypingSpeed)
            {
                int amountLetters = Mathf.Clamp(Mathf.RoundToInt(typeInterval / TypingSpeed), 0, LeftText.Length);

                typeInterval = 0;
                string newText = LeftText.Substring(0, amountLetters);
                SetText(newText);
                LeftText = LeftText.Remove(0, newText.Length);

                if (audioSource != null)
                {
                    if (returnAudioClip != null && (newText.IndexOf("\r") > -1 || newText.IndexOf("\n") > -1))
                    {
                        audioSource.PlayOneShot(returnAudioClip);
                    }
                    else if (letterAudioClip != null)
                    {
                        audioSource.PlayOneShot(letterAudioClip);
                    }
                }
            }
        }

        private void SetText(string text)
        {
            textComponent.text += text;
        }


    }
}
