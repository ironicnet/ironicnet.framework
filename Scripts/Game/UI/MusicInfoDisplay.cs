﻿using UnityEngine;
using System.Collections;


namespace Ironicnet.Framework.Game.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class MusicInfoDisplay : MonoBehaviour
    {

        public Effects.TypeWriterEffect PlayerInfoTyper;
        protected CanvasGroup Group;

        [Multiline]
        public string DisplayFormat = "Title: %TRACKNAME%\rName: %ARTIST%";
        // Use this for initialization
        void Start()
        {
            Group = this.GetComponent<CanvasGroup>();
        }
        public float FadeStep = 0.1f;
        protected int Status = 0;
        protected float displayCounter;
        protected float fadeCounter;
        protected float FadeTime;
        protected float DisplayTime;
        // Update is called once per frame
        void Update()
        {

            //Showing
            if (Status == 1)
            {
                fadeCounter += Time.deltaTime;
                if (fadeCounter >= FadeTime)
                {
                    Group.alpha += FadeStep;
                    if (Group.alpha >= 1)
                    {
                        Group.alpha = 1;
                        Status = 2;
                        fadeCounter = 0;
                        displayCounter = 0;
                    }
                }
            }
            //Visible
            else if (Status == 2)
            {
                displayCounter += Time.deltaTime;
                if (displayCounter >= DisplayTime)
                {
                    fadeCounter = 0;
                    displayCounter = 0;
                    Status = 3;
                }
            }
            //Hiding
            else if (Status == 3)
            {
                fadeCounter += Time.deltaTime;
                if (fadeCounter >= FadeTime)
                {
                    Group.alpha -= FadeStep;
                    if (Group.alpha <= 0)
                    {
                        Group.alpha = 0;
                        Status = 4;
                        fadeCounter = 0;
                        displayCounter = 0;
                    }
                }
            }
            //Hidden
            else
            {
                Group.alpha = 0;
            }

        }

        public void Show(Ironicnet.Framework.Game.MusicPlayer.MusicInfo musicInfo, float displayTime, float visibleThreshold, float fadeStep)
        {

            Status = 1;
            DisplayTime = displayTime;
            FadeTime = visibleThreshold;
            FadeStep = fadeStep;
            PlayerInfoTyper.StartTyping(DisplayFormat.Replace("%TRACKNAME%", musicInfo.TrackName).Replace("%ARTIST%", musicInfo.Artist) + "\r");
        }
    }

}